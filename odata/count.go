package odata

import "strconv"

func ParseCountString(count string) (*ODataCountQuery, error) {
	i, err := strconv.ParseBool(count)
	if err != nil {
		return nil, err
	}

	result := ODataCountQuery(i)

	return &result, nil
}
