package odata

import "strconv"

func ParseTopString(top string) (*ODataTopQuery, error) {
	i, err := strconv.Atoi(top)
	result := ODataTopQuery(i)
	return &result, err
}

func ParseSkipString(skip string) (*ODataSkipQuery, error) {
	i, err := strconv.Atoi(skip)
	result := ODataSkipQuery(i)
	return &result, err
}
