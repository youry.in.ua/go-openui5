package odata

import (
	"net/http"
	"net/url"
	"strings"
)

const (
	ODataFieldContext string = "@odata.context"
	ODataFieldCount   string = "@odata.count"
	ODataFieldValue   string = "value"
)

type ODataProvider interface {
	// Request a single entity from the provider. Should return a response field
	// that contains the value mapping properties to values for the entity.
	GetEntity(*ODataRequest) (*ODataResponseField, error)
	// Request a collection of entities from the provider. Should return a
	// response field that contains the value of a slice of every entity in the
	// collection filtered by the request query parameters.
	GetEntityCollection(*ODataRequest) (*ODataResponseField, error)
	// Request the number of entities in a collection, disregarding any filter
	// query parameters.
	GetCount(*ODataRequest) (int, error)
	// Get the object model representation from the provider.
	GetMetadata() *ODataMetadata
}

type ODataService struct {
	// The base url for the service. Navigating to this URL will display the
	// service document.
	BaseUrl *url.URL
	// The provider for this service that is serving the data to the OData API.
	Provider ODataProvider
	// Metadata cache taken from the provider.
	Metadata *ODataMetadata
	// A mapping from schema names to schema references
	SchemaLookup map[string]*ODataSchema
	// A bottom-up mapping from entity type names to schema namespaces to
	// the entity type reference
	EntityTypeLookup map[string]map[string]*ODataEntityType
	// A bottom-up mapping from entity container names to schema namespaces to
	// the entity container reference
	EntityContainerLookup map[string]map[string]*ODataEntityContainer
	// A bottom-up mapping from entity set names to entity collection names to
	// schema namespaces to the entity set reference
	EntitySetLookup map[string]map[string]map[string]*ODataEntitySet
	// A lookup for entity properties if an entity type is given, lookup
	// properties by name
	PropertyLookup map[*ODataEntityType]map[string]*ODataProperty
	// A lookup for navigational properties if an entity type is given,
	// lookup navigational properties by name
	NavigationPropertyLookup map[*ODataEntityType]map[string]*ODataNavigationProperty
}

type providerChannelResponse struct {
	Field *ODataResponseField
	Error error
}

func BuildService(provider ODataProvider, serviceUrl string) (*ODataService, error) {
	metadata := provider.GetMetadata()

	// build the lookups from the metadata
	schemaLookup := map[string]*ODataSchema{}
	entityLookup := map[string]map[string]*ODataEntityType{}
	containerLookup := map[string]map[string]*ODataEntityContainer{}
	entitySetLookup := map[string]map[string]map[string]*ODataEntitySet{}
	propertyLookup := map[*ODataEntityType]map[string]*ODataProperty{}
	navPropLookup := map[*ODataEntityType]map[string]*ODataNavigationProperty{}

	for _, schema := range metadata.DataServices.Schemas {
		schemaLookup[schema.Namespace] = schema

		for _, entity := range schema.EntityTypes {
			if _, ok := entityLookup[entity.Name]; !ok {
				entityLookup[entity.Name] = map[string]*ODataEntityType{}
			}
			if _, ok := propertyLookup[entity]; !ok {
				propertyLookup[entity] = map[string]*ODataProperty{}
			}
			if _, ok := navPropLookup[entity]; !ok {
				navPropLookup[entity] = map[string]*ODataNavigationProperty{}
			}
			entityLookup[entity.Name][schema.Namespace] = entity

			for _, prop := range entity.Properties {
				propertyLookup[entity][prop.Name] = prop
			}
			for _, prop := range entity.NavigationProperties {
				navPropLookup[entity][prop.Name] = prop
			}
		}

		for _, container := range schema.EntityContainers {
			if _, ok := containerLookup[container.Name]; !ok {
				containerLookup[container.Name] = map[string]*ODataEntityContainer{}
			}
			containerLookup[container.Name][schema.Namespace] = container

			for _, set := range container.EntitySets {
				if _, ok := entitySetLookup[set.Name]; !ok {
					entitySetLookup[set.Name] = map[string]map[string]*ODataEntitySet{}
				}
				if _, ok := entitySetLookup[set.Name][container.Name]; !ok {
					entitySetLookup[set.Name][container.Name] = map[string]*ODataEntitySet{}
				}
				entitySetLookup[set.Name][container.Name][schema.Namespace] = set
			}
		}
	}

	parsedUrl, err := url.Parse(serviceUrl)

	if err != nil {
		return nil, err
	}

	return &ODataService{
		parsedUrl,
		provider,
		provider.GetMetadata(),
		schemaLookup,
		entityLookup,
		containerLookup,
		entitySetLookup,
		propertyLookup,
		navPropLookup,
	}, nil
}

func (service *ODataService) ODataHTTPHandler(w http.ResponseWriter, r *http.Request) {

	request, err := ParseRequest(r.URL.Path, r.URL.Query())

	if err != nil {
		panic(err) // TODO: return proper error
	}

	// Semanticize all tokens in the request, connecting them with their
	// corresponding types in the service
	err = SemanticizeRequest(request, service)

	if err != nil {
		panic(err) // TODO: return proper error
	}

	// TODO: differentiate GET and POST requests
	var response []byte = []byte{}
	if request.RequestKind == RequestKindMetadata {
		response, err = service.buildMetadataResponse(request)
	} else if request.RequestKind == RequestKindService {
		response, err = service.buildServiceResponse(request)
	} else if request.RequestKind == RequestKindCollection {
		response, err = service.buildCollectionResponse(request)
	} else if request.RequestKind == RequestKindEntity {
		response, err = service.buildEntityResponse(request)
	} else if request.RequestKind == RequestKindProperty {
		response, err = service.buildPropertyResponse(request)
	} else if request.RequestKind == RequestKindPropertyValue {
		response, err = service.buildPropertyValueResponse(request)
	} else if request.RequestKind == RequestKindCount {
		response, err = service.buildCountResponse(request)
	} else if request.RequestKind == RequestKindRef {
		response, err = service.buildRefResponse(request)
	} else {
		err = NotImplementedError("Request type not understood.")
	}

	if err != nil {
		panic(err) // TODO: return proper error
	}

	w.Write(response)
}

func (service *ODataService) LookupEntityType(name string) (*ODataEntityType, error) {
	// strip "Collection()" and just return the raw entity type
	// The provider should keep track of what are collections and what aren't
	if strings.Contains(name, "(") && strings.Contains(name, ")") {
		name = name[strings.Index(name, "(")+1 : strings.LastIndex(name, ")")]
	}

	parts := strings.Split(name, ".")
	entityName := parts[len(parts)-1]
	// remove entity from the list of parts
	parts = parts[:len(parts)-1]

	schemas, ok := service.EntityTypeLookup[entityName]
	if !ok {
		return nil, BadRequestError("Entity " + name + " does not exist.")
	}

	if len(parts) > 0 {
		// namespace is provided
		entity, ok := schemas[parts[len(parts)-1]]
		if !ok {
			return nil, BadRequestError("Entity " + name + " not found in given namespace.")
		}
		return entity, nil
	} else {
		// no namespace, just return the first one

		// throw error if ambiguous
		if len(schemas) > 1 {
			return nil, BadRequestError("Entity " + name + " is ambiguous. Please provide a namespace.")
		}

		for _, v := range schemas {
			return v, nil
		}
	}

	// If this happens, that's very bad
	return nil, BadRequestError("No schema lookup found for entity " + name)
}

func (service *ODataService) LookupEntitySet(name string) (*ODataEntitySet, error) {
	parts := strings.Split(name, ".")
	setName := parts[len(parts)-1]
	// remove entity set from the list of parts
	parts = parts[:len(parts)-1]

	containers, ok := service.EntitySetLookup[setName]
	if !ok {
		return nil, BadRequestError("Entity set " + name + " does not exist.")
	}

	if len(parts) > 0 {
		// container is provided
		schemas, ok := containers[parts[len(parts)-1]]
		if !ok {
			return nil, BadRequestError("Container " + name + " not found.")
		}

		// remove container name from the list of parts
		parts = parts[:len(parts)-1]

		if len(parts) > 0 {
			// schema is provided
			set, ok := schemas[parts[len(parts)-1]]
			if !ok {
				return nil, BadRequestError("Entity set " + name + " not found.")
			}
			return set, nil
		} else {
			// no schema is provided

			if len(schemas) > 1 {
				// container is ambiguous
				return nil, BadRequestError("Entity set " + name + " is ambiguous. Please provide fully qualified name.")
			}

			// there should be one schema, if not then something went horribly wrong
			for _, set := range schemas {
				return set, nil
			}
		}

	} else {
		// no container is provided

		// return error if entity set is ambiguous
		if len(containers) > 1 {
			return nil, BadRequestError("Entity set " + name + " is ambiguous. Please provide fully qualified name.")
		}

		// find the first schema, it will be the only one
		for _, schemas := range containers {
			if len(schemas) > 1 {
				// container is ambiguous
				return nil, BadRequestError("Entity set " + name + " is ambiguous. Please provide fully qualified name.")
			}

			// there should be one schema, if not then something went horribly wrong
			for _, set := range schemas {
				return set, nil
			}
		}
	}
	return nil, BadRequestError("Entity set " + name + " not found.")
}

func (service *ODataService) buildMetadataResponse(request *ODataRequest) ([]byte, error) {
	return service.Metadata.Bytes()
}

func (service *ODataService) buildServiceResponse(request *ODataRequest) ([]byte, error) {
	// TODO
	return nil, NotImplementedError("Service responses are not implemented yet.")
}

func (service *ODataService) buildCollectionResponse(request *ODataRequest) ([]byte, error) {
	response := &ODataResponse{Fields: map[string]*ODataResponseField{}}
	// get request from provider
	responses := make(chan *providerChannelResponse)
	go func() {
		result, err := service.Provider.GetEntityCollection(request)
		responses <- &providerChannelResponse{result, err}
		close(responses)
	}()

	if bool(*request.Query.Count) {
		// if count is true, also include the count result
		counts := make(chan *providerChannelResponse)

		go func() {
			result, err := service.Provider.GetCount(request)
			counts <- &providerChannelResponse{&ODataResponseField{result}, err}
			close(counts)
		}()

		r := <-counts
		if r.Error != nil {
			return nil, r.Error
		}

		response.Fields[ODataFieldCount] = r.Field
	}
	// build context URL
	context := request.LastSegment.SemanticReference.(*ODataEntitySet).Name
	path, err := url.Parse("./$metadata#" + context)
	if err != nil {
		return nil, err
	}
	contextUrl := service.BaseUrl.ResolveReference(path).String()
	response.Fields[ODataFieldContext] = &ODataResponseField{Value: contextUrl}

	// wait for a response from the provider
	r := <-responses

	if r.Error != nil {
		return nil, r.Error
	}

	response.Fields[ODataFieldValue] = r.Field

	return response.Json()
}

func (service *ODataService) buildEntityResponse(request *ODataRequest) ([]byte, error) {
	// get request from provider
	responses := make(chan *providerChannelResponse)
	go func() {
		result, err := service.Provider.GetEntity(request)
		responses <- &providerChannelResponse{result, err}
		close(responses)
	}()

	// build context URL
	context := request.LastSegment.SemanticReference.(*ODataEntitySet).Name
	path, err := url.Parse("./$metadata#" + context + "/$entity")
	if err != nil {
		return nil, err
	}
	contextUrl := service.BaseUrl.ResolveReference(path).String()

	// wait for a response from the provider
	r := <-responses

	if r.Error != nil {
		return nil, r.Error
	}

	// Add context field to result and create the response
	switch r.Field.Value.(type) {
	case map[string]*ODataResponseField:
		fields := r.Field.Value.(map[string]*ODataResponseField)
		fields[ODataFieldContext] = &ODataResponseField{Value: contextUrl}
		response := &ODataResponse{Fields: fields}

		return response.Json()
	default:
		return nil, InternalServerError("Provider did not return a valid response" +
			" from GetEntity()")
	}
}

func (service *ODataService) buildPropertyResponse(request *ODataRequest) ([]byte, error) {
	// TODO
	return nil, NotImplementedError("Property responses are not implemented yet.")
}

func (service *ODataService) buildPropertyValueResponse(request *ODataRequest) ([]byte, error) {
	// TODO
	return nil, NotImplementedError("Property value responses are not implemented yet.")
}

func (service *ODataService) buildCountResponse(request *ODataRequest) ([]byte, error) {
	// get request from provider
	responses := make(chan *providerChannelResponse)
	go func() {
		result, err := service.Provider.GetCount(request)
		responses <- &providerChannelResponse{&ODataResponseField{result}, err}
		close(responses)
	}()

	// wait for a response from the provider
	r := <-responses

	if r.Error != nil {
		return nil, r.Error
	}

	return r.Field.Json()
}

func (service *ODataService) buildRefResponse(request *ODataRequest) ([]byte, error) {
	// TODO
	return nil, NotImplementedError("Ref responses are not implemented yet.")
}
