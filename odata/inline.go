package odata

const (
	ALLPAGES = "allpages"
	NONE     = "none"
)

func ParseInlineCountString(inlinecount string) (*ODataInlineCountQuery, error) {
	result := ODataInlineCountQuery(inlinecount)
	if inlinecount == ALLPAGES {
		return &result, nil
	} else if inlinecount == NONE {
		return &result, nil
	} else {
		return nil, BadRequestError("Invalid inlinecount query.")
	}
}
