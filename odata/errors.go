package odata

type ODataError struct {
	ResponseCode int
	Message      string
}

func (err *ODataError) Error() string {
	return err.Message
}

func BadRequestError(message string) *ODataError {
	return &ODataError{400, message}
}

func NotFoundError(message string) *ODataError {
	return &ODataError{404, message}
}

func MethodNotAllowedError(message string) *ODataError {
	return &ODataError{405, message}
}

func GoneError(message string) *ODataError {
	return &ODataError{410, message}
}

func PreconditionFailedError(message string) *ODataError {
	return &ODataError{412, message}
}

func InternalServerError(message string) *ODataError {
	return &ODataError{500, message}
}

func NotImplementedError(message string) *ODataError {
	return &ODataError{501, message}
}
