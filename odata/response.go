package odata

import (
	"bytes"
	"strconv"
)

type ODataResponseField struct {
	Value interface{}
}

func (f *ODataResponseField) Json() ([]byte, error) {
	switch f.Value.(type) {
	case string:
		return prepareJsonString([]byte(f.Value.(string)))
	case []byte:
		return prepareJsonString(f.Value.([]byte))
	case int:
		return []byte(strconv.Itoa(f.Value.(int))), nil
	case float64:
		return []byte(strconv.FormatFloat(f.Value.(float64), 'f', -1, 64)), nil
	case map[string]*ODataResponseField:
		return prepareJsonDict(f.Value.(map[string]*ODataResponseField))
	case []*ODataResponseField:
		return prepareJsonList(f.Value.([]*ODataResponseField))
	default:
		return nil, InternalServerError("Response field type not recognized.")
	}
}

type ODataResponse struct {
	Fields map[string]*ODataResponseField
}

func (r *ODataResponse) Json() ([]byte, error) {
	result, err := prepareJsonDict(r.Fields)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func prepareJsonDict(d map[string]*ODataResponseField) ([]byte, error) {
	var buf bytes.Buffer
	buf.WriteByte('{')
	count := 0
	for k, v := range d {
		buf.WriteByte('"')
		buf.Write([]byte(k))
		buf.WriteByte('"')
		buf.WriteByte(':')
		field, err := v.Json()
		if err != nil {
			return nil, err
		}
		buf.Write(field)
		count++
		if count < len(d) {
			buf.WriteByte(',')
		}
	}
	buf.WriteByte('}')
	return buf.Bytes(), nil
}

func prepareJsonString(s []byte) ([]byte, error) {
	// escape double quotes
	s = bytes.Replace(s, []byte("\""), []byte("\\\""), -1)
	var buf bytes.Buffer
	buf.WriteByte('"')
	buf.Write(s)
	buf.WriteByte('"')
	return buf.Bytes(), nil
}

func prepareJsonList(l []*ODataResponseField) ([]byte, error) {
	var buf bytes.Buffer
	buf.WriteByte('[')
	count := 0
	for _, v := range l {
		field, err := v.Json()
		if err != nil {
			return nil, err
		}
		buf.Write(field)
		count++
		if count < len(l) {
			buf.WriteByte(',')
		}
	}
	buf.WriteByte(']')
	return buf.Bytes(), nil
}
