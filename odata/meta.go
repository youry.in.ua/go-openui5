package odata

import "encoding/xml"

const (
	ODataString         = "Edm.String"
	ODataInt16          = "Edm.Int16"
	ODataInt32          = "Edm.Int32"
	ODataInt64          = "Edm.Int64"
	ODataDecimal        = "Edm.Decimal"
	ODataBinary         = "Edm.Binary"
	ODataBoolean        = "Edm.Boolean"
	ODataTimeOfDay      = "Edm.TimeOfDay"
	ODataDate           = "Edm.Date"
	ODataDateTimeOffset = "Edm.DateTimeOffset"
)

type ODataMetadata struct {
	XMLName      xml.Name `xml:"edmx:Edmx"`
	XMLNamespace string   `xml:"xmlns:edmx,attr"`
	Version      string   `xml:"Version,attr"`
	DataServices *ODataServices
	References   []*ODataReference
}

type ODataServices struct {
	XMLName xml.Name `xml:"edmx:DataServices"`
	Schemas []*ODataSchema
}

type ODataSchema struct {
	XMLName          xml.Name `xml:"Schema"`
	Namespace        string   `xml:"Namespace,attr"`
	Alias            string   `xml:"Alias,attr,omitempty"`
	Actions          []*ODataAction
	Annotations      []*ODataAnnotations
	Annotation       []*ODataAnnotation
	ComplexTypes     []*ODataComplexType
	EntityContainers []*ODataEntityContainer
	EntityTypes      []*ODataEntityType
	EnumTypes        []*ODataEnumType
	Functions        []*ODataFunction
	Terms            []*ODataTerm
	TypeDefinitions  []*ODataTypeDefinition
}

type ODataAction struct {
	XMLName       xml.Name `xml:"Action"`
	Name          string   `xml:"Name,attr"`
	IsBound       string   `xml:"IsBound,attr,omitempty"`
	EntitySetPath string   `xml:"EntitySetPath,attr,omitempty"`
	Parameters    []*ODataParameter
	ReturnType    *ODataReturnType
}

type ODataAnnotations struct {
	XMLName     xml.Name `xml:"Annotations"`
	Target      string   `xml:"Target,attr"`
	Qualifier   string   `xml:"Qualifier,attr,omitempty"`
	Annotations []*ODataAnnotation
}

type ODataAnnotation struct {
	XMLName   xml.Name `xml:"Annotation"`
	Term      string   `xml:"Term,attr"`
	Qualifier string   `xml:"Qualifier,attr,omitempty"`
}

type ODataComplexType struct {
	XMLName              xml.Name `xml:"ComplexType"`
	Name                 string   `xml:"Name,attr"`
	BaseType             string   `xml:"BaseType,attr,omitempty"`
	Abstract             string   `xml:"Abstract,attr,omitempty"`
	OpenType             string   `xml:"OpenType,attr,omitempty"`
	Properties           []*ODataProperty
	NavigationProperties []*ODataNavigationProperty
}

type ODataEntityContainer struct {
	XMLName         xml.Name `xml:"EntityContainer"`
	Name            string   `xml:"Name,attr"`
	Extends         string   `xml:"Extends,attr,omitempty"`
	EntitySets      []*ODataEntitySet
	Singletons      []*ODataSingleton
	ActionImports   []*ODataActionImport
	FunctionImports []*ODataFunctionImport
}

type ODataEntityType struct {
	XMLName              xml.Name `xml:"EntityType"`
	Name                 string   `xml:"Name,attr"`
	BaseType             string   `xml:"BaseType,attr,omitempty"`
	Abstract             string   `xml:"Abstract,attr,omitempty"`
	OpenType             string   `xml:"OpenType,attr,omitempty"`
	HasStream            string   `xml:"HasStream,attr,omitempty"`
	Key                  *ODataKey
	Properties           []*ODataProperty
	NavigationProperties []*ODataNavigationProperty
}

type ODataEnumType struct {
	XMLName        xml.Name `xml:"EnumType"`
	Name           string   `xml:"Name,attr"`
	UnderlyingType string   `xml:"UnderlyingType,attr,omitempty"`
	IsFlags        string   `xml:"IsFlags,attr,omitempty"`
	Members        []*ODataMember
}

type ODataFunction struct {
	XMLName       xml.Name `xml:"Function"`
	Name          string   `xml:"Name,attr"`
	IsBound       string   `xml:"IsBound,attr,omitempty"`
	IsComposable  string   `xml:"IsComposable,attr,omitempty"`
	EntitySetPath string   `xml:"EntitySetPath,attr,omitempty"`
	Parameters    []*ODataParameter
	ReturnType    *ODataReturnType
}

type ODataTypeDefinition struct {
	XMLName        xml.Name `xml:"TypeDefinition"`
	Name           string   `xml:"Name,attr"`
	UnderlyingType string   `xml:"UnderlyingTypeattr,omitempty"`
	Annotations    []*ODataAnnotation
}

type ODataParameter struct {
	XMLName   xml.Name `xml:"Parameter"`
	Name      string   `xml:"Name,attr"`
	Type      string   `xml:"Type,attr"`
	Nullable  string   `xml:"Nullable,attr,omitempty"`
	MaxLength int      `xml:"MaxLength,attr,omitempty"`
	Precision int      `xml:"Precision,attr,omitempty"`
	Scale     int      `xml:"Scale,attr,omitempty"`
	SRID      string   `xml:"SRID,attr,omitempty"`
}

type ODataReturnType struct {
	XMLName   xml.Name `xml:"ReturnType"`
	Name      string   `xml:"Name,attr"`
	Type      string   `xml:"Type,attr"`
	Nullable  string   `xml:"Nullable,attr,omitempty"`
	MaxLength int      `xml:"MaxLength,attr,omitempty"`
	Precision int      `xml:"Precision,attr,omitempty"`
	Scale     int      `xml:"Scale,attr,omitempty"`
	SRID      string   `xml:"SRID,attr,omitempty"`
}

type ODataProperty struct {
	XMLName      xml.Name `xml:"Property"`
	Name         string   `xml:"Name,attr"`
	Type         string   `xml:"Type,attr"`
	Nullable     string   `xml:"Nullable,attr,omitempty"`
	MaxLength    int      `xml:"MaxLength,attr,omitempty"`
	Precision    int      `xml:"Precision, attr,omitempty"`
	Scale        int      `xml:"Scale,attr,omitempty"`
	Unicode      string   `xml:"Unicode,attr,omitempty"`
	SRID         string   `xml:"SRID,attr,omitempty"`
	DefaultValue string   `xml:"DefaultValue,attr,omitempty"`
}

type ODataNavigationProperty struct {
	XMLName                xml.Name `xml:"NavigationProperty"`
	Name                   string   `xml:"Name,attr"`
	Type                   string   `xml:"Type,attr"`
	Nullable               string   `xml:"Nullable,attr,omitempty"`
	Partner                string   `xml:"Partner,attr,omitempty"`
	ContainsTarget         string   `xml:"ContainsTarget,attr,omitempty"`
	ReferentialConstraints []*ODataReferentialConstraint
}

type ODataReferentialConstraint struct {
	XMLName            xml.Name       `xml:"ReferentialConstraint"`
	Property           string         `xml:"Property,attr"`
	ReferencedProperty string         `xml:"ReferencedProperty,attr"`
	OnDelete           *ODataOnDelete `xml:"OnDelete,omitempty"`
}

type ODataOnDelete struct {
	XMLName xml.Name `xml:"OnDelete"`
	Action  string   `xml:"Action,attr"`
}

type ODataEntitySet struct {
	XMLName                    xml.Name `xml:"EntitySet"`
	Name                       string   `xml:"Name,attr"`
	EntityType                 string   `xml:"EntityType,attr"`
	IncludeInServiceDocument   string   `xml:"IncludeInServiceDocument,attr,omitempty"`
	NavigationPropertyBindings []*ODataNavigationPropertyBinding
}

type ODataNavigationPropertyBinding struct {
	XMLName xml.Name `xml:"NavigationPropertyBinding"`
	Path    string   `xml:"Path,attr"`
	Target  string   `xml:"Target,attr"`
}

type ODataSingleton struct {
	XMLName                    xml.Name `xml:"Singleton"`
	Name                       string   `xml:"Name,attr"`
	Type                       string   `xml:"Type,attr"`
	NavigationPropertyBindings []*ODataNavigationPropertyBinding
}

type ODataActionImport struct {
	XMLName   xml.Name `xml:"ActionImport"`
	Name      string   `xml:"Name,attr"`
	Action    string   `xml:"Action,attr"`
	EntitySet string   `xml:"EntitySet,attr,omitempty"`
}

type ODataFunctionImport struct {
	XMLName                  xml.Name `xml:"FunctionImport"`
	Name                     string   `xml:"Name,attr"`
	Function                 string   `xml:"Function,attr"`
	EntitySet                string   `xml:"EntitySet,attr,omitempty"`
	IncludeInServiceDocument string   `xml:"IncludeInServiceDocument,attr,omitempty"`
}

type ODataKey struct {
	XMLName     xml.Name `xml:"Key"`
	PropertyRef *ODataPropertyRef
}

type ODataPropertyRef struct {
	XMLName xml.Name `xml:"PropertyRef"`
	Name    string   `xml:"Name,attr"`
}

type ODataMember struct {
	XMLName xml.Name `xml:"Member"`
	Name    string   `xml:"Name,attr"`
	Value   string   `xml:"Value,attr,omitempty"`
}

type ODataTerm struct {
	XMLName      xml.Name `xml:"Term"`
	Name         string   `xml:"Name,attr"`
	Type         string   `xml:"Type,attr"`
	BaseTerm     string   `xml:"BaseTerm,attr,omitempty"`
	DefaultValue string   `xml:"DefaultValue,attr,omitempty"`
	AppliesTo    string   `xml:"AppliesTo,attr,omitempty"`
}

type ODataReference struct {
	XMLName            xml.Name `xml:"edmx:Reference"`
	Uri                string   `xml:"Uri,attr"`
	Includes           []*ODataInclude
	IncludeAnnotations []*ODataIncludeAnnotations
}

type ODataInclude struct {
	XMLName   xml.Name `xml:"edmx:Include"`
	Namespace string   `xml:"Namespace,attr"`
	Alias     string   `xml:"Alias,attr,omitempty"`
}

type ODataIncludeAnnotations struct {
	XMLName         xml.Name `xml:"edmx:IncludeAnnotations"`
	TermNamespace   string   `xml:"TermNamespace,attr"`
	Qualifier       string   `xml:"Qualifier,attr,omitempty"`
	TargetNamespace string   `xml:"TargetNamespace,attr,omitempty"`
}

func (t *ODataMetadata) Bytes() ([]byte, error) {
	output, err := xml.MarshalIndent(t, "", "    ")
	if err != nil {
		return nil, err
	}

	return append([]byte(xml.Header), output...), nil
}
