package odata

func ParseApplyString(apply string) (*ODataApplyQuery, error) {
	result := ODataApplyQuery(apply)
	return &result, nil
}
