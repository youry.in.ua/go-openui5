package odata

type ODataIdentifier map[string]string

const (
	RequestKindUnknown int = iota
	RequestKindMetadata
	RequestKindService
	RequestKindEntity
	RequestKindCollection
	RequestKindSingleton
	RequestKindProperty
	RequestKindPropertyValue
	RequestKindRef
	RequestKindCount
)

const (
	SemanticTypeUnknown int = iota
	SemanticTypeEntity
	SemanticTypeEntitySet
	SemanticTypeDerivedEntity
	SemanticTypeAction
	SemanticTypeFunction
	SemanticTypeProperty
	SemanticTypePropertyValue
	SemanticTypeRef
	SemanticTypeCount
	SemanticTypeMetadata
)

type ODataRequest struct {
	FirstSegment *ODataSegment
	LastSegment  *ODataSegment
	Query        *ODataQuery
	RequestKind  int
}

type ODataSegment struct {
	// The raw segment parsed from the URI
	RawValue string

	// The kind of resource being pointed at by this segment
	SemanticType int

	// A pointer to the metadata type this object represents, as defined by a
	// particular service
	SemanticReference interface{}

	// The name of the entity, type, collection, etc.
	Name string

	// map[string]string of identifiers passed to this segment. If the identifier
	// is not key/value pair(s), then all values will be nil. If there is no
	// identifier, it will be nil.
	Identifier *ODataIdentifier

	// The next segment in the path.
	Next *ODataSegment
	// The previous segment in the path.
	Prev *ODataSegment
}

type ODataQuery struct {
	Filter      *ODataFilterQuery
	Apply       *ODataApplyQuery
	Expand      *ODataExpandQuery
	Select      *ODataSelectQuery
	OrderBy     *ODataOrderByQuery
	Top         *ODataTopQuery
	Skip        *ODataSkipQuery
	Count       *ODataCountQuery
	InlineCount *ODataInlineCountQuery
	Search      *ODataSearchQuery
	Format      *ODataFormatQuery
}

type ODataFilterQuery struct {
	Tree *ParseNode
}

type ODataApplyQuery string

type ODataExpandQuery struct {
	ExpandItems []*ExpandItem
}

type ODataSelectQuery struct {
	SelectItems []*SelectItem
}

type ODataOrderByQuery struct {
	OrderByItems []*OrderByItem
}

type ODataTopQuery int

type ODataSkipQuery int

type ODataCountQuery bool

type ODataInlineCountQuery string

type ODataSearchQuery struct {
	Tree *ParseNode
}

type ODataFormatQuery struct {
}
