package main

import "gitlab.com/youry.in.ua/go-openui5/odata"

type PgProvider struct {
}

// Request a single entity from the provider. Should return a response field
// that contains the value mapping properties to values for the entity.
func (p *PgProvider) GetEntity(req *odata.ODataRequest) (*odata.ODataResponseField, error) {
	return &odata.ODataResponseField{}, nil
}

// Request a collection of entities from the provider. Should return a
// response field that contains the value of a slice of every entity in the
// collection filtered by the request query parameters.
func (p *PgProvider) GetEntityCollection(req *odata.ODataRequest) (*odata.ODataResponseField, error) {
	return &odata.ODataResponseField{}, nil
}

// Request the number of entities in a collection, disregarding any filter
// query parameters.
func (p *PgProvider) GetCount(req *odata.ODataRequest) (int, error) {
	return 0, nil
}

// Get the object model representation from the provider.
func (p *PgProvider) GetMetadata() *odata.ODataMetadata {
	return &odata.ODataMetadata{}
}
