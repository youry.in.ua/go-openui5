package main

import (
	"encoding/json"
	"flag"
	"log"
	"net/http"

	"github.com/boltdb/bolt"
	"github.com/hoisie/mustache"
	"gitlab.com/youry.in.ua/go-openui5/odata"
)

type node struct {
	source string
	templ  *mustache.Template
	ctx    map[string]string
}

var dictionary = map[string]*node{}

var context = map[string]string{}

type impl struct {
	db  *bolt.DB
	app string
	od  *odata.ODataService
}

func main() {
	port := flag.String("p", "8100", "port to serve on")
	directory := flag.String("d", ".", "the directory of static file to host")
	boltFile := flag.String("b", "app.db", "boltDB file")
	app := flag.String("a", "app", "application")
	flag.Parse()

	i := &impl{}

	if db, err := bolt.Open(*boltFile, 0600, &bolt.Options{ReadOnly: true}); err != nil {
		log.Fatal(err)
	} else {
		i.db = db
		i.app = *app
		i.od, _ = odata.BuildService(&PgProvider{}, "")
	}
	defer i.db.Close()

	initDict(i)

	http.Handle("/resources/", http.FileServer(http.Dir(*directory)))

	http.HandleFunc("/", i.routeNode)
	http.HandleFunc("/odata", i.od.ODataHTTPHandler)
	http.HandleFunc("/model/", i.routeNode)
	http.HandleFunc("/view/", i.routeNode)
	http.HandleFunc("/controller/", i.routeNode)
	http.HandleFunc("/css/", i.routeNode)
	http.HandleFunc("/i18n/", i.routeNode)

	log.Printf("Serving %s on HTTP port: %s\n", *directory, *port)
	log.Fatal(http.ListenAndServe(":"+*port, nil))
}

func (i *impl) routeNode(w http.ResponseWriter, r *http.Request) {

	if node, ok := dictionary[r.URL.Path]; ok {
		//log.Println(node)
		response := node.templ.Render(node.ctx)
		w.Write([]byte(response))
	} else {
		http.Redirect(w, r, r.URL.Path, http.StatusFound)
	}

}

func initDict(i *impl) {

	var src []byte

	i.db.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte(i.app))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			//fmt.Printf("key=%s, value=%s\n", k, v)

			sk := string(k)
			src = make([]byte, len(v))
			copy(src, v)
			if sk == "context.json" {
				json.Unmarshal(src, &context)
			} else {
				if sk == "index.html" {
					dictionary["/"] = &node{source: string(src)}
				} else {
					dictionary["/"+sk] = &node{source: string(src)}
				}
			}
		}

		return nil
	})

	for k, v := range dictionary {
		dictionary[k].ctx = context
		dictionary[k].templ, _ = mustache.ParseString(v.source)
	}
}
