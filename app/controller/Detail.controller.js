sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";
	return Controller.extend("sap.ui.app.ats.controller.Detail", {
		onInit: function () {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("detail").attachPatternMatched(this._onObjectMatched, this);
		},
		_onObjectMatched: function (oEvent) {
			var sPath = oEvent.getParameter("arguments").invoicePath.replace(/-/g, "/");
			//console.log("sPath:" + sPath);
			this.getView().bindElement({
				path: sPath,
				model: "invoice"
			});
		}
	});
});