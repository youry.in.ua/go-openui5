package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"github.com/boltdb/bolt"
)

func main() {
	rootDir := flag.String("d", "app", "loaded dir")
	boltFile := flag.String("b", "app.db", "boltDB file")
	flag.Parse()

	db, err := bolt.Open(*boltFile, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(*rootDir))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		return nil
	})

	os.Chdir(*rootDir)

	err1 := filepath.Walk(".", func(path string, f os.FileInfo, err error) error {
		fmt.Println(path, f.IsDir())

		if !f.IsDir() {
			if content, err := ioutil.ReadFile(path); err == nil {
				db.Update(func(tx *bolt.Tx) error {
					b := tx.Bucket([]byte(*rootDir))

					err := b.Put([]byte(path), content)
					return err
				})
			}
		}
		return nil
	})

	log.Printf("filepath.Walk() returned %v\n", err1)

}
